<div class="col-lg-3 col-md-6">
    <div>
        <h5 class="sidebar-title">Tautan Cepat</h5>
        <div class="list-group list-group-flush">
            <a href="{{ route('site.beranda') }}" class="list-group-item list-group-item-action">Home</a>
            <a href="https://www.politanisamarinda.ac.id/" class="list-group-item list-group-item-action">Direktorat</a>
            <a href="http://jti.politanisamarinda.ac.id/" class="list-group-item list-group-item-action">Jurusan</a>
            <a href="http://sia.politanisamarinda.ac.id/login.php" class="list-group-item list-group-item-action">Portal Akademik</a>
            <a href="http://repository.politanisamarinda.ac.id/" class="list-group-item list-group-item-action">Repository</a>
            <a href="https://pddikti.kemdikbud.go.id/data_prodi/ODlDNjY5MzgtOTBDNS00OThDLTk5MzgtQzNCQTA5QTg0ODVB/20221" class="list-group-item list-group-item-action">Pddikti</a>
            <a href="{{ route('site.faq') }}" class="list-group-item list-group-item-action">F.A.Q</a>
        </div>
    </div>
</div>
